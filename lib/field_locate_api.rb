require 'faraday'
require 'faraday_middleware'
require 'active_support/all'
require 'field_locate_api/version'

Dir[File.expand_path('../faraday/*.rb', __dir__)].each { |f| require f }
require File.expand_path('field_locate_api/configuration', __dir__)
require File.expand_path('field_locate_api/api', __dir__)
require File.expand_path('field_locate_api/client', __dir__)
require File.expand_path('field_locate_api/error', __dir__)

module FieldLocateApi
  extend Configuration
  # Alias for FieldLocateApi::Client.new
  # @return [FieldLocateApi::Client]
  def self.client(options = {})
    FieldLocateApi::Client.new(options)
  end

  # Delegate to FieldLocateApi::Client
  def self.method_missing(method, *args, &block)
    return super unless client.respond_to?(method)
    client.send(method, *args, &block)
  end
end
