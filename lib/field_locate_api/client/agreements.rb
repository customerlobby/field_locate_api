module FieldLocateApi
  class Client
    module Agreements
      def agreements(params = {})
        get('agreement', params)
      end

      def agreement(id, params = {})
        get("agreement/#{id}", params)
      end
    end
  end
end
