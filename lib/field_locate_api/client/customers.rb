module FieldLocateApi
  class Client
    module Customers
      def customers(params = {})
        response = get('customer', params)
      end

      def customer(id, params = {})
        response = get("customer/#{id}", params)
      end
    end
  end
end
