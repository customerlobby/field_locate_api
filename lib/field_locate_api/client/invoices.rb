module FieldLocateApi
  class Client
    module Invoices
      def invoices(params = {})
        get('invoice', params)
      end

      def invoice(id, params = {})
        get("invoice/#{id}", params)
      end
    end
  end
end
