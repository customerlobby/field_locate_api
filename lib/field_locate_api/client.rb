module FieldLocateApi
  # Wrapper for the FieldLocateApi REST API.
  class Client < API
    Dir[File.expand_path('client/*.rb', __dir__)].each { |f| require f }

    include FieldLocateApi::Client::Customers
    include FieldLocateApi::Client::Invoices
    include FieldLocateApi::Client::Agreements
  end
end
