require 'faraday_middleware'
Dir[File.expand_path('../faraday/*.rb', __dir__)].each { |f| require f }

module FieldLocateApi
  module Connection
    private

    def connection
      options = {
        url: "#{endpoint}#{api_version}/"
      }

      Faraday::Connection.new(options) do |connection|
        connection.use FaradayMiddleware::Mashify
        connection.use Faraday::Response::ParseJson
        connection.adapter(adapter)
        connection.use Faraday::Response::Logger, logger if logger
      end
    end
  end
end
