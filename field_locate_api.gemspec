
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'field_locate_api/version'

Gem::Specification.new do |spec|
  spec.name          = 'field_locate_api'
  spec.version       = FieldLocateApi::VERSION
  spec.authors       = ['Customer Lobby']
  spec.email         = ['dev@customerlobby.com']
  spec.description   = 'Client for the FieldLocate API'
  spec.summary       = 'Client for the FieldLocate API'
  spec.homepage      = ''
  spec.license       = 'MIT'

  spec.files         = `git ls-files`.split($INPUT_RECORD_SEPARATOR)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.3'
  spec.add_development_dependency('pry')
  spec.add_development_dependency 'rake'
  spec.add_development_dependency('rspec', '~> 3.1')
  spec.add_development_dependency('webmock', '~> 1.6')

  spec.add_runtime_dependency('activesupport')
  spec.add_runtime_dependency('faraday')
  spec.add_runtime_dependency('faraday_middleware')
  spec.add_runtime_dependency('hashie')
  spec.add_runtime_dependency('nokogiri')
  spec.add_runtime_dependency('vcr')
end
