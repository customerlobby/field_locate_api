require 'spec_helper'
require 'pry'

RSpec.describe FieldLocateApi do
  after do
    FieldLocateApi.reset
  end

  describe '.client' do
    it 'should be a FieldLocateApi::Client' do
      expect(FieldLocateApi.client).to be_a(FieldLocateApi::Client)
    end
  end

  describe '#api_key' do
    it 'should return the default api key' do
      expect(FieldLocateApi.api_key).to eq(FieldLocateApi::Configuration::DEFAULT_API_KEY)
    end
  end

  describe '#api_key=' do
    it 'should set the api key' do
      FieldLocateApi.api_key = 'test'
      expect(FieldLocateApi.api_key).to eq('test')
    end
  end

  describe '#api_version' do
    it 'should return the default api version' do
      expect(FieldLocateApi.api_version).to eq(FieldLocateApi::Configuration::DEFAULT_API_VERSION)
    end
  end

  describe '#api_version=' do
    it 'should set the api_version' do
      FieldLocateApi.api_version = '/test'
      expect(FieldLocateApi.api_version).to eq('/test')
    end
  end

  describe '#adapter' do
    it 'should return the default adapter' do
      expect(FieldLocateApi.adapter).to eq(FieldLocateApi::Configuration::DEFAULT_ADAPTER)
    end
  end

  describe '#adapter=' do
    it 'should set the adapter' do
      FieldLocateApi.adapter = :typhoeus
      expect(FieldLocateApi.adapter).to eq(:typhoeus)
    end
  end

  describe '#endpoint' do
    it 'should return the default endpoint' do
      expect(FieldLocateApi.endpoint).to eq(FieldLocateApi::Configuration::DEFAULT_ENDPOINT)
    end
  end

  describe '#endpoint=' do
    it 'should set the endpoint' do
      FieldLocateApi.endpoint = 'http://www.google.com'
      expect(FieldLocateApi.endpoint).to eq('http://www.google.com')
    end
  end

  describe '#configure' do
    FieldLocateApi::Configuration::VALID_OPTIONS_KEYS.each do |key|
      it "should set the #{key}" do
        FieldLocateApi.configure do |config|
          config.send("#{key}=", key)
          expect(FieldLocateApi.send(key)).to eq(key)
        end
      end
    end
  end
end
